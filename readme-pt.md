# Introdução

## Aula 1 - O curso e o instrutor
- Apresentação
- Agenda

https://www.udemy.com/course/gitlab-auto-devops/

https://gitlab.com/auto-devops-ninja

https://www.udemy.com/user/jonathan-dias-baraldi/

## Aula 2 - AutoDevOps - Que bicho é esse?

https://docs.gitlab.com/ee/topics/autodevops/

https://docs.gitlab.com/ee/topics/autodevops/cloud_deployments/auto_devops_with_gke.html


## Aula 3 - Kubernetes
- Containers
- Kubernetes

containers-kubernetes.pdf

https://kubernetes.io

https://kubernetes.io/docs/concepts/overview/

https://kubernetes.io/docs/concepts/workloads/

	- Rede
	- Pods
	- Containers
	- Service
	- Ingress
	- Volume

## Aula 4 - Conta no Google Cloud

- Iremos usar o GKE - Google Kubernetes Engine
- Criar um cluster de exemplo para mostrar como iremos logar e trabalhar nele

- GCP
	https://cloud.google.com/gartner-cloud-infrastructure-as-a-service?hl=pt-br

	https://cloud.google.com/infrastructure?hl=pt-br

	https://cloud.google.com/kubernetes-engine

- Requisitos mínimos
	- 1 conta no GCP e 1 conta no Gitlab.
	- 1 dominio - DNS
	- cluster kubernetes GKE


## Aula 5 - Gitlab/Repositório.
- Como é o repositório e como ele funciona

https://gitlab.com/auto-devops-ninja


## Aula 6 - Infraestrutura

	Nessa aula iremos ver qual infraestrutura iremos usar.

	Quais ferrramentas
		- Google Cloud
		- GKE
		- CloudDNS
		- Nginx
		- Cert-manager
		- Sentry
		- Cloud Monitoring
		- Gitlab
		- 

	Qual o objetivo do curso 

![alt text](arquitetura.png)


## Aula 7 - Aplicação
	Falar sobre a aplicação Nodejs que iremos usar, ver o código.

https://gitlab.com/auto-devops-ninja/nodejs

# Infraestrutura

## Aula 8 - Kubernetes
Criar o cluster e deixar ele pronto para ser usado.

## Aula 9 - Domínio
Criar dominio no freenom e deixar pronto para ser usado
Configurar com o CloudDNS

## Aula 10 - Runner
Mostrar como Instalar o Runner


```sh
helm repo add gitlab https://charts.gitlab.io

kubectl create namespace gitlab-runner
```

Abrir o cloud-shell do google, criar o arquivo **values.yaml** para poder rodar o comando do helm depois. Você deve alterar o valor do **runnerRegistrationToken**, para o token gerado para o seu grupo.


```sh
$ vi values.yaml

```

```yaml
gitlabUrl : "https://gitlab.com/"
runnerRegistrationToken : "<USAR_O_SEU_TOKEN>"
rbac:
  create: true
  clusterWideAccess: true


runners:
  config: |
    [[runners]]
      [runners.kubernetes]
        image = "ubuntu:20.04"
        privileged = true
      [[runners.kubernetes.volumes.empty_dir]]
        name = "docker-certs"
        mount_path = "/certs/client"
        medium = "Memory"
```
Após o arquivo ser salvo, rodar os comandos para instalar o Runner no seu grupo.

```sh
helm install --namespace gitlab-runner gitlab-runner -f values.yaml gitlab/gitlab-runner

# Comando para upgrade
# helm upgrade --namespace gitlab-runner gitlab-runner -f values.yaml gitlab/gitlab-runner

kubectl create role access-secrets --verb=get,list,watch,update,create --resource=secrets

kubectl create rolebinding --role=access-secrets default-to-secrets --serviceaccount=kube-system:default

kubectl create clusterrolebinding permissive-binding   --clusterrole=cluster-admin   --user=admin   --user=kubelet   --group=system:serviceaccounts

```


## Aula 11 - Gitlab Agent
Como instalar o Gitlab Agent

Para instalar o Gitlab Agent, iremos usar como base o repositório: 

- https://gitlab.com/auto-devops-ninja/kubernetes-agent

Nele, tem um arquivo chamado:

- **.gitlab/agents/agent1/config.yaml**

Dentro dele, temos:


```yaml
ci_access:
    groups:
        - id: "<SEU_GRUPO>"
    projects:
        - id: "<SEU_GRUPO>/cluster-management"

gitops:
    manifest_projects:
        - id : "<SEU_GRUPO>/<SEU_PROJETO>"

```

Você deve editar seu arquivo e trocar pelo nome do seu grupo. Com isso o Kubernetes Agent consegue dar permissão para rodar os pipelines nos outros projetos no mesmo grupo.
Esse passo é fundamental. Feito isso, vá na área de Kubernetes Agents e peça para gerar o codigo para você, que será igual o código abaixo, trocando apenas o **TOKEN**.


```sh
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install agent1 gitlab/gitlab-agent \
    --namespace gitlab-agent \
    --create-namespace \
    --set image.tag=v15.2.0 \
    --set config.token=<SEU_TOKEN_AQUI> \
    --set config.kasAddress=wss://kas.gitlab.com
````

Após fazer a instalação, acessar o cluster e os containers e ver os logs para validar se está tudo certo. 


## Aula 12 - Cluster Management - Ingress
Como instalar o Ingress com o Cluster Management

RRepositório
https://gitlab.com/auto-devops-ninja/cluster-management

Iremos editar o arquivo na raiz do diretório:

**helmfile.yaml**

e descomentar a linha do ingress, ficando o restante comentado. 

**- path: applications/ingress/helmfile.yaml**

Salvar e comitar e ver o pipeline aplicando o ingress.
Iremos pegar o IP Publico, e adicionar no DNS, criando uma zona  com wildcard no Google Cloud, no serviço de DNS.


### Aula 13 - Cluster Management - Certificado

Como instalar o certificado com o Cluster Management




# Aplicação

https://docs.gitlab.com/ee/topics/autodevops/cloud_deployments/auto_devops_with_gke.html#enable-auto-devops-and-run-the-pipeline


## Aula 14 - Deploy da aplicação
Explicar a aplicação que vamos fazer deploy, nodejs

- Variaveis de ambiente, uso do kube_context
- Enviroments, staging e prod

## Aula 15 - Pipeline - AutoDevOps

- Explicar as etapas para rodar ele 
- Fszer o deploy e acessar as urls


## Aula 16 - Customizar AutoDevOps
	- YAML e objetos como postgres
	- URL De produção e staging

https://docs.gitlab.com/ee/topics/autodevops/customize.html

.gitlab/auto-deploy-values.yaml


# DevOps

## Aula 17 - Monitoramento - Google Cloud
	
## Aula 18 - Logs - Google Cloud

## Aula 19 - Sentry - Configuração da APP
- Configurar o App e fazer deploy

## Aula 20 - Sentry - Monitoramento
- Ver o que o Sentry nos mostra sobre os erro, monitoramento e performance da aplicação


# Aplicação parte 2

## Aula 21 - Fazer novo branch para simular uma correção 

# Revisão

## Aula 22 - Revisão e Roadmap

- Aplicação Python
- Instalar Gitlab on-promise
- Instalar Kubernetes on-promise
	- Rancher com monitoramento
- Instalar Sentry on-promise
